#ifndef UTILS_HPP_
#define UTILS_HPP_

class Utils {
public:
    static const int randi(const int a, const int b);
    static const double randf(const int a, const int b);
};

#endif  // UTILS_HPP_