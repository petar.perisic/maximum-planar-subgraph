#ifndef SIMULATED_ANNEALING_HPP_
#define SIMULATED_ANNEALING_HPP_

#include "./graph.hpp"

#include <boost/graph/adjacency_list.hpp>

class SimulatedAnnealing {
public:
    const Graph solve(Graph G);

private:
    unsigned m_max_evaluations;
};

#endif  // SIMULATED_ANNEALING_HPP_