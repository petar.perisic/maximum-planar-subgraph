#ifndef GRAPH_HPP_
#define GRAPH_HPP_

#include <boost/graph/adjacency_list.hpp>
#include <vector>

typedef boost::adjacency_list<
    boost::vecS,
    boost::vecS,
    boost::undirectedS,
    boost::property<boost::vertex_index_t, int>
> graph_t;

class Graph {
public:
    Graph();
    Graph(const int number_of_vertices);
    Graph(const std::string & file_path);
    Graph(const Graph & other);

    Graph& operator=(const Graph & other);

    void add_vertex(const int vertex);
    void remove_vertex(const int vertex);

    void add_edge(const int vertex_a, const int vertex_b);
    void add_edge(const std::pair<int, int> & edge);
    
    void remove_edge(const int vertex_a, const int vertex_b);
    void remove_edge(const std::pair<int, int> & edge);

    bool is_planar() const;

    const int number_of_edges() const;
    const int number_of_vertices() const;
    
    const std::vector<std::pair<int, int>> edges() const;

    const std::pair<int, int> random_edge() const;

private:
    graph_t m_instance;
};

#endif  // GRAPH_HPP_