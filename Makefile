CC       = g++
PROGRAM  = maximum_planar_subgraph
BOOST    = boost_program_options
CXXFLAGS = -std=c++17 -Wall
OBJECTS  = graph.o simulated_annealing.o utils.o

$(PROGRAM): src/main.cpp $(OBJECTS)
	$(CC) $(CXXFLAGS) -o $@ $^ -l $(BOOST)

graph.o: src/graph.cpp include/graph.hpp include/utils.hpp
	$(CC) $(CXXFLAGS) -c -o $@ $<

simulated_annealing.o: src/simulated_annealing.cpp include/simulated_annealing.hpp include/graph.hpp include/utils.hpp
	$(CC) $(CXXFLAGS) -c -o $@ $<

utils.o: src/utils.cpp include/utils.hpp
	$(CC) $(CXXFLAGS) -c -o $@ $<

.PHONY: clean

clean:
	rm *.o $(PROGRAM)
