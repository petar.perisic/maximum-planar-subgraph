#include "../include/utils.hpp"

#include <random>
#include <cstdlib>
#include <iostream>

const int Utils::randi(const int a, const int b)
{
    std::srand(time(nullptr));
    if (a == b) 
        return 0;
    else
        return (std::rand() % (b - a)) + a;
}

const double Utils::randf(const int a, const int b)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis((float) a, (float) b);
    
    return dis(gen);
}

