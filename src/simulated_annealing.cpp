#include "../include/utils.hpp"
#include "../include/graph.hpp"
#include "../include/simulated_annealing.hpp"

#include <random>
#include <iostream>
#include <cmath>

const Graph SimulatedAnnealing::solve(Graph G)
{
    const int V = G.number_of_vertices();
    const int E = G.number_of_edges();

    unsigned current_eval = 0;

    if (V <= 25) {
        m_max_evaluations = 51 * (V + E);
    } else {
        m_max_evaluations = 7 * (V + E);
    }

    Graph best_value;
    Graph current_value;

    unsigned iter = 1;

    if (G.is_planar())
        return G;

    while (current_eval < m_max_evaluations) {
        const auto random_edge = G.random_edge(); 

        current_value.add_edge(random_edge);
        
        if (current_value.is_planar()) {
            G.remove_edge(random_edge);
            current_eval++;
        } else {
            const auto swap_edge = current_value.random_edge();
            current_value.remove_edge(swap_edge);

            if (current_value.is_planar()) {
                G.remove_edge(random_edge);
                G.add_edge(swap_edge);
                current_eval++;
            } else {
                const double p = 1 / std::sqrt(iter);
                const double q = Utils::randf(0, 1);

                if (p > q) {
                    current_value.remove_edge(random_edge);
                    G.add_edge(swap_edge);
                    current_eval++;
                } else {
                    current_value.add_edge(swap_edge);
                    current_value.remove_edge(random_edge);
                }
            }
        }

        if (current_value.number_of_edges() >= best_value.number_of_edges())
            best_value = current_value;

        if (best_value.number_of_edges() == 3 * V - 6)
            return best_value;

        if (current_value.number_of_edges() == 0)
            return best_value;

        iter++;
    }

    return best_value;
}
