#include "../include/graph.hpp"
#include "../include/utils.hpp"

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/boyer_myrvold_planar_test.hpp>

#include <iostream>
#include <fstream>
#include <vector>

#include <ctime>
#include <cstdlib>

Graph::Graph()
    : m_instance()
{}

Graph::Graph(const int number_of_vertices)
    : m_instance(number_of_vertices)
{}

Graph::Graph(const std::string &  file_path)
{
    std::ifstream file(file_path);
    
    unsigned vertices = 0, edges = 0; 
    file >> vertices >> edges;
    
    m_instance = graph_t(vertices);

    unsigned vertex_a = 0, vertex_b = 0;
    while (file >> vertex_a >> vertex_b) {
        boost::add_edge(vertex_a - 1, vertex_b - 1, m_instance);
    }
    
    file.close();
}

Graph::Graph(const Graph & other)
    : m_instance()
{
    for (const auto & edge : other.edges())
        boost::add_edge(edge.first, edge.second, m_instance);
}

Graph& Graph::operator=(const Graph & other)
{
    m_instance.m_edges.clear();

    for (const auto & edge : other.m_instance.m_edges)
        boost::add_edge(edge.m_source, edge.m_target, m_instance);

    return *this;
}

void Graph::add_vertex(const int vertex)
{
    boost::add_vertex(vertex, m_instance);
}

void Graph::remove_vertex(const int vertex)
{
    boost::remove_vertex(vertex, m_instance);
}

void Graph::add_edge(const int vertex_a, const int vertex_b)
{
    boost::add_edge(vertex_a, vertex_b, m_instance);
}

void Graph::add_edge(const std::pair<int, int> & edge)
{
    boost::add_edge(edge.first, edge.second, m_instance);
}

void Graph::remove_edge(const int vertex_a, const int vertex_b)
{
    boost::remove_edge(vertex_a, vertex_b, m_instance);
}

void Graph::remove_edge(const std::pair<int, int> & edge)
{
    boost::remove_edge(edge.first, edge.second, m_instance);
}

bool Graph::is_planar() const
{
    return boost::boyer_myrvold_planarity_test(m_instance);
}

const int Graph::number_of_edges() const
{
    return m_instance.m_edges.size();
}

const int Graph::number_of_vertices() const
{
    return m_instance.m_vertices.size();
}

const std::vector<std::pair<int, int>> Graph::edges() const
{
    std::vector<std::pair<int, int>> edges;
    for (const auto & edge : m_instance.m_edges)
        edges.push_back(std::pair<int, int>(edge.m_source, edge.m_target));

    return edges;
}

const std::pair<int, int> Graph::random_edge() const
{
    return edges()[Utils::randi(0, edges().size())];
}
