#include "../include/graph.hpp"
#include "../include/simulated_annealing.hpp"

#include <iostream>
#include <fstream>
#include <string>

#include <chrono> 

int main(int argc, char const *argv[])
{
    std::string file_path(argv[1]);
    Graph G(file_path);
   
    SimulatedAnnealing sa;
    auto start = std::chrono::high_resolution_clock::now();
    auto solution = sa.solve(G);
    auto stop = std::chrono::high_resolution_clock::now();
    
    auto duration = std::chrono::duration_cast<std::chrono::seconds>(stop - start);

    // std::ofstream out_file;
    // std::string out_path = "./test/results/" + file_path.substr(file_path.find_last_of("/") + 1);

    // out_file.open(out_path, std::ios_base::app);
    // out_file << solution.number_of_edges() << " " << duration.count() << std::endl; 
    // out_file.close();

    std::cout << "|E| = " << solution.number_of_edges() << std::endl;
    std::cout << "time = " << duration.count() << std::endl;
    
    return 0;
}
