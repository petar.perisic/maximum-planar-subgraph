# Rešavanje problema maksimalnog planarnog podgrafa
  
## Seminarski rad u okviru kursa **`` Računarska inteligencija ``**.

  Problem: Neka je dat graf *`` G = (V, E) ``*, pronaći maksimalan planaran podgraf
  *`` G' = (V, E') ``*, pri čemu mora da važi *`` |E'| <= |E| ``*.

  Problem je rešavan primenom **``simuliranog kaljenja``**.

### Za kompilaciju programa potrebna je **``boost``** biblioteka

```bash
  sudo apt-get install libboost-all-dev
```

## Kompilacija i pokretanje programa

```bash
    make
    ./maximum_planar_subgraph [file_path]
```

### Korišćeni test podaci se nalaze u **``test/planar-data.tar.gz``**.
