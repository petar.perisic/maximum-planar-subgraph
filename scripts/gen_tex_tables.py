import subprocess
import os

table_path = "../../tex-table.txt"

if __name__ == "__main__":
    os.chdir("../test/parsed_results")
    for root, dirs, files in os.walk("."):
        for file in sorted(files):
            with open(file, "r") as f:
                data = f.readlines()

                file_name = data[0].strip()
                V = data[1].strip()
                E = data[2].strip()
                best_edge_case = data[3].strip()
                avg_edges = data[4].strip()
                best_time = data[5].strip()
                avg_time = data[6].strip()
                
                line = "{0} & {1} & {2} & {3} \\\\ \n".format(
                        file_name, V, E, best_edge_case
                )
                
                with open(table_path, "a+") as t:
                    t.write("{0} & {1} & {2} & 0 & {3} & 0 & {4} \\\\ \n".format(
                        file_name, V, E, avg_time, best_edge_case
                    ))
