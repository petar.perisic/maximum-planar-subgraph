import subprocess
import os

if __name__ == "__main__":
    os.chdir("../")

    tests = [ ]

    test_instances = 5

    for i in range(len(tests)):
        subprocess.call("clear")
        print("> Currently running {0}\n\t> Total {1}/{2}".format(tests[i], i + 1, len(tests)))
        for j in range(test_instances):
            subprocess.call(["./maximum_planar_subgraph", "./test/data/" + tests[i]])

    os.chdir("./scripts")
