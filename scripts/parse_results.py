import subprocess
import os

if __name__ == "__main__":
    os.chdir("../test/results")
    for root, dirs, files in os.walk("."):
        for file in files:
            with open(file, "r") as f:
                data = f.readlines()
                
                best_edge_case = -1
                best_time = -1

                total_edges = 0
                total_time = 0

                for line in data:
                    tokens = line.strip().split(" ")
                    if tokens[0] == "":
                        continue

                    edges = int(tokens[0])
                    time = int(tokens[1])
                    
                    total_edges += edges
                    total_time += time

                    if edges > best_edge_case:
                        best_edge_case = edges 

                    if time > best_time:
                        best_time = time

                avg_edges = round(total_edges / len(data), 2)
                avg_time = round(total_time / len(data), 2)

                V = 0
                E = 0
                with open("../data/{0}".format(file), "r") as g:
                    line = g.readline().strip().split(" ")
                    line = list(filter(lambda x: bool(x), line))
                    V = int(line[0])
                    E = int(line[1])

                with open("../parsed_results/{0}".format(file), "w+") as pr:
                    pr.write("{0}\n{1}\n{2}\n{3}\n{4}\n{5}\n{6}".format(
                        file[0:file.rfind(".")], V, E, best_edge_case, avg_edges, best_time, avg_time
                    ))
