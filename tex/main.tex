% !TEX encoding = UTF-8 Unicode

\documentclass[a4paper]{article}

\usepackage{color}
\usepackage{url}

\usepackage[medium]{lmodern}

\usepackage[T1]{fontenc}

\usepackage{graphicx}

\usepackage{subfig}

\usepackage[english, serbian]{babel}
\usepackage[utf8]{inputenc}

\usepackage[unicode]{hyperref}
\hypersetup{colorlinks,citecolor=green,filecolor=green,linkcolor=blue,urlcolor=blue}

\usepackage{listings}

\usepackage{float}

\newtheorem{theorem}{Teorema}

\usepackage[vlined,ruled]{algorithm2e}
\renewcommand{\algorithmcfname}{Algoritam}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ 
    backgroundcolor=\color{white},
    basicstyle=\scriptsize\ttfamily,
    breakatwhitespace=false,
    breaklines=true,
    captionpos=b,
    commentstyle=\color{mygreen},
    deletekeywords={...},
    escapeinside={\%*}{*)},
    extendedchars=true,
    firstnumber=1000,
    frame=single,
    keepspaces=true,
    keywordstyle=\color{blue},
    language=Python,
    morekeywords={*,...},
    numbers=left,
    numbersep=5pt,
    numberstyle=\tiny\color{mygray},
    rulecolor=\color{black},
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    stepnumber=2,
    stringstyle=\color{mymauve},
    tabsize=4,
    title=\lstname
}

\begin{document}

\title{
    Rešavanje problema maksimalnog planarnog podgrafa primenom simuliranog kaljenja \\
    \small{
        Seminarski rad u okviru kursa \\
        Računarska inteligencija \\ 
        Matematički fakultet
    }
}

\author{
    Nemanja Gružanić 420/2016 \\
    Petar Perišić 266/2015 
}

\maketitle

\abstract{
    U ovom radu navodimo navodimo heuristiku za rešavanje problema maksimalnog planarnog
    podgrafa koja koristi metodu simuliranog kaljenja.
    Ova heuristika je već korišćena za rešavanje navedenog problema,
    međutim u ovom radu će biti opisano dodatno uopštenje heuristike i postignuti
    rezultati.
}

\tableofcontents

\clearpage

\section{ Uvod }

\subsection{ Opis i pristup problemu }

Problem maksimalnog planarnog podgrafa \emph{ (eng. Maximum Planar Subgraph Problem, MPS) } 
je definisan na sledeći način.
    Neka je dat graf $ G = (V, E) $,
    pronaći maksimalan planaran podgraf $ G' = (V, E') $, pri čemu mora da važi $ E' \subseteq E $.
Graf je planaran ukoliko se može nacrtati tako da se nikoje dve grane ne seku, u suprotnom
graf nije planaran. Ovaj problem pripada grupi \emph{NP-teških} problema, stoga potrebne su dobre
heuristike kako bise pronašle dobre aproksimacije ovog problema.


Problem maksimalnog planarnog podgrafa ima veliki značaj za crtanje grafova i može se primeniti u drugim
problemima, poput problema rasporeda objekata.


\subsection{ Pregled korišćene literature }

Postoje brojni naučni radovi, samim tim i razvijene heuristike, koje se bave rešavanjem problema 
maksimalnog planarnog podgrafa. U ovom radu, datom problemu pristupamo korišćenjem simuliranog
kaljenja. Simulirano kaljenje se pokazalo kao dobar pristup za optimizaciju ovog problema, u smislu
vremenske i prostorne složenosti algoritma, što ćemo i pokušati da prikažemo.


Većina literature na osnovu koje je baziran ovaj rad je vezan za primenu simuliranog kaljenja na 
dati problem. Ovaj rad je većinom baziran prvenstveno na \cite{timo1}, kao i \cite{timo2}.
U prvom radu prikazan je način na koji se može primeniti simulirano kaljenje na problem maksimalnog planarnog podgrafa. 
Drugi rad prikazuje efikasna dva algoritma za rešavanje
problema, kao i pohlepni algoritam. Kako smo u okviru kursa radili simulirano kaljenje
odlučili smo da pristupimo problemu koristeći datu metodu.


Detaljnije o rezultatima koje smo postigli i poređenje sa rezultatima iz navedenih
naučnih radova u sekciji \ref{sec:results}.


\section{ Rešavanje problema korišćenjem simuliarnog kaljenja }

U ovoj sekciji ćemo detaljnije opisati naš pristup rešavanju problema,
heuristiku koju smo koristili i način implementacije. Pre toga ćemo navesti osnovne 
teoreme koje imaju veliki značaj za rešavanje problema.

\subsection{ Planarnost grafova }
\label{sec:graph-theory}

Kao što smo već naveli, graf $ G = (V, E) $ je planaran ukoliko ga možemo nacrtati tako da
se nikoje dve grane ne seku, pri čemu mora da važi pri čemu mora da važi $ E' \subseteq E $, 
tj. ukoliko je graf moguće predstaviti u ravni tako da je zajednička tačka dve grane
može biti samo čvor koji predstavlja zajedničku krajnju tačku tih grana. Na slici 
\ref{fig:graph-planarity} se može videti primer planarnog grafa i grafa koji nije planaran.


\begin{figure}[H]
    \centering
    \subfloat[ Planaran graf ]{{ \includegraphics[height=3.5cm]{images/planar-graph.png} }}
    \qquad
    \subfloat[ Neplanaran graf ]{{ \includegraphics[width=3.5cm]{images/non-planar-graph.png} }}
    \caption{ Planarnost grafova }
    \label{fig:graph-planarity}
\end{figure}


Dve najznačajnije teoreme za planarnost grafova je Ojlerova teorema za planarne grafove
i teorema Kuratovskog \cite{graph-theory}. Ove dve teoreme imaju veliki uticaj i značaj 
za rešavanje problema maksimalnog podgrafa. 

\begin{theorem} [Ojlerova teorema]
    \label{theorem:Euler}
    Povezan, planaran graf sa $ n $ čvorova i $ m $ grana deli ravan na $ f = m - n + 2$ 
    oblasti.
\end{theorem}

\begin{theorem} [Kuratovski 1930]
    Graf je planaran ako i samo ako ne sadrži kao delimični podgraf graf $ K_5 $ i
    graf $ K_{3,3} $.
\end{theorem}


Grafovi $ K_5 $ i $ K_{3, 3} $ se mogu videti na slici \ref{fig:important-graphs}.

\begin{figure}[H]
    \centering
    \subfloat[ Graf $ K_5 $ ]{{ \includegraphics[width=3cm]{images/k_5.png} }}
    \qquad
    \subfloat[ Graf $ K_{3, 3} $ ]{{ \includegraphics[height=2.6cm]{images/k_3_3.png} }}
    \caption{ Važni grafovi }
    \label{fig:important-graphs}
\end{figure}

Pored navedenih teorema, navešćemo još dve važne osobine planarnih grafova koje slede
iz navedenih teorema.


Za planaran graf $ G = (V, E) $ sa $ v $ čvorova i $ e $ grana, važi sledeće:
\begin{enumerate}
    \item Ako je $ v \geq 3 $ onda je $ e \leq 3v - 6 $
    \item Ako je $ v \geq 3 $ i ne postoji ciklus dužine 3, onda je $ e \leq 2v - 4 $ 
\end{enumerate}



Jedan od važnijih delova algoritma koji utiče na složenost samog algoritma,
čini sam test planarnosti grafa. Test planarnosti grafa predstavlja ispitivanje
da li je graf planaran ili ne. Razvijeni su razni efikasni algoritmi koji 
ispituju planarnost grafa. Trenutno, najbolji algoritam za ispitivanje planarnosti grafa
ima linearnu vremensku složenost. Međutim, većina algoritama koji su razvijeni za ispitivanje
planarnosti grafova su teški za implementaciju.

Kako sam test planarnosti grafa
predstavlja pomoćnu funkciju u algoritmu za rešavanje problema, a kako bi postigli
što veću efikasnost algoritma i kako bi postigli što bolje rezultate odlučili smo da
koristimo biblioteku koja nam omogućuje efikasan test planarnosti. 
Opis linearnog algoritma za test planarnosti i više detalja u vezi implementacije
algoritma navodimo u sekciji \ref{sec:implementation}.

\subsection{ Simulirano kaljenje }

Algoritam simuliranog kaljenja \emph{ (eng. Simulated Annealing, SA) } u svom izvornom obliku
je zasnovan na analogiji između kaljenja čelika i rešavanje velikih kombinatornih
optimizacionih problema. Odatle i potiče naziv metode ''simulirano kaljenje''.
Metoda je zasnovana na lokalnom pretraživanju, uz mehanizam koji omogućava izlazak iz lokalnog
optimuma. Početkom 1980.-ih tri \emph{IBM}-ova inžinjera su uveli koncept kaljenja u kombinatornim
optimizacionim problemima \cite{optimization-simulated-annealing}.

Pri procesu kaljenja čelika cilj je oplemenjivanje metala tako da on postane čvršći. Kako bi se 
postigla čvrstoća metala, potrebno je zagrevati čelik do određene temperature, zatim nakon
kratkog zadržavanja na toj temperaturi, započeti postepeno hlađenje. Kako ne bi došlo do pucanja
metala, potrebno je voditi računa o brzini hlađenja, jer brzo hlađenje može da uzrokuje pucanje
metala.


Osnovna ideja simuliranog kaljenja je da se na osnovu inicijalnog rešenja, koje se generiše na slučajan
ili neki određen način, poboljša njegov kvalitet. 


Inicijalno rešenje na početku proglašavamo za najbolje
rešenje. Kvalitet rešenja se poboljšava tako što u svakoj iteraciji biramo neko rešenje iz
okoline trenutnog rešenja. Okolinu trenutnog rešenja predstavlja skup rešenja koji je jako blizu
trenutnom rešenju. Ukoliko je neko rešenje iz okoline bolje od trenutnog rešenja njega proglašavamo
za najbolje rešenje. Pod određenim uslovima, možemo prihvatiti novo rešenje čak iako njegova vrednost
nije bolja od trenutne. Na taj način se smanjuje verovatnoća konvergencije algoritma ka lokalnom
optimumu.


Algoritam simuliranog kaljenja se zaustavlja kada se ispuni kriterijum zaustavljanja. Kriterijumi
zaustavljanja mogu biti razni, na primer ukoliko je dostignuto maksimalno vreme izvršavanja,
zatim izvršavanje maksimalnog broja iteracija algoritma ili maksimalanog broja evaluacija funkcije cilja itd.
Takođe, mogući su neki specifični kriterijumi zaustavljanja.


Sledi pseudokod opšteg algoritma simuliranog kaljenja.
\begin{figure}[H]
\centering
\begin{minipage}{0.8\linewidth}

\begin{algorithm}[H]
\label{alg:sa-intro}

\SetAlgoLined

\caption{ Opšti algoritam simuliranog kaljenja }
    generiši početno rešenje $ s $ \\
    inicijalizuj vrednost najboljeg rešenja $ f^* = f(s) $ \\
\While {nije zadovoljen kriterijum zaustavljanja} {
    izaberi proizvoljno rešenje $ s' $ u okolini rešenja s \\
    
    \uIf{ $f(s') < f(s) $ }{
        $ s = s' $  \
    } \\
    
     \uIf{ $f(s') \geq f(s) $ } {
        ažuriraj vrednost opadajuće funkcije p \\
        izaberi proizvoljno $ q \in [0,1] $ \\
        \uIf{ $ p > q $} {
            $ s = s' $
        }
    } \\
    
    \uIf{  $f(s') < f^* $ } {
        $ f^* = f(s') $
    } \\
}
\end{algorithm}
    \end{minipage}
\end{figure}

\subsection{ Primena simuliranog kaljenja na problem \\ maksimalnog planarnog podgrafa  }
\label{sec:annealing-impl}

Glavna ideja algoritma za rešavanje problema maksimalnog planarnog podgrafa se svodi na održavanje
dva skupa grana. Skup $ E_1 $, koji je inicijalizovan kao prazan i skup $ E_2 $  koji sadrži sve
grane ulaznog grafa. Prvi skup grana na kraju izvršavanja algoritma zapravo predstavlja skup grana
koji ima maksimalni planarni podgraf. 

Nakon inicijalizacije, kako bi se povećavao broj grana u skupu $ E_1 $, bira se slučajna grana $ e_2 $
iz skupa $ E_2 $. Ukoliko je graf koji ima skup grana $ E_1 $ sa dodatnom
granom $ e_2 $ planaran, tada dodajemo granu $ e_2 $ skupu $ E_1 $ i granu $ e_2 $ izbacujemo iz skupa $ E_2 $.
Ukoliko $ E_1 $ sa granom $ e_2 $ ne zadovoljava uslov planarnosti, bira se slučajna grana $ e_1 $ iz
skupa $ E_1 $. Zatim menjamo grane $ e_1 $ i $ e_2 $ i ispitujemo planarnost takvog skupa
grana $ E_1 $. Ukoliko je zadovoljen uslov planarnosti, menjamo date dve grane. Ako
planarnost nije zadovoljena, onda proveravamo da li možemo da izbacimo granu $ e_1 $. Proveru
postižemo tako što generišemo slučajan broj $ q $, $ 0 \leq q \leq 1 $ i ukoliko važi 
$ q < \frac{1}{\sqrt{i}} $, gde je $ i $ broj trenutne iteracije, tada
izbacujemo granu $ e_1 $ iz $ E_1 $ i vraćamo je u skup $ E_2 $.
U ovom delu algoritma zapravo smanjujemo veličinu skupa $ E_1 $ \cite{timo1}. 


Postupak ponavljamo sve dok se ne ispuni kriterijum zaustavljanja.


Algoritam simuliranog kaljenja za rešavanje problema maksimalnog planarnog podgrafa je opisan u
algoritmu \ref{alg:sa-mps}.


\begin{figure}[H]
\centering
\begin{minipage}{0.95\linewidth}

\begin{algorithm}[H]
\label{alg:sa-mps}
\caption{ Algoritam simuliranog kaljenja za rešavanje problema maksimalnog planarnog podgrafa }

\SetAlgoLined

\hspace{\algorithmicindent} \textbf{Ulaz:}  Neplanaran graf $ G = (V, E) $ \\ 
\hspace{\algorithmicindent} \textbf{Izlaz:} Planaran graf   $ G' = (V, E') $ \\ 
    
    inicijalizuj $ E_1 = \emptyset $ \\
    inicijalizuj $ E_2 = E $ \\
    
    \While {nije zadovoljen kriterijum zaustavljanja} {
        izaberi slučajnu granu $ e_2 $ iz $ E_2 $ \\
        
        \uIf{ $ E_1 \cup \{ e_2 \} \ planaran $ } {
            postavi $ E_1 = E_1 \cup \{ e_2 \} $ \\
            postavi $ E_2 = E_2 \setminus \{ e_2 \} $ \\
        } \uElse {
            izaberi slučajnu granu $ e_1 $ iz $ E_1 $ \\
           
            \uIf{ $ (E_1 \setminus \{ e_1 \}) \cup \{ e_2 \} \ planaran $ } {
                postavi $ E_1 = (E_1 \setminus \{ e_1 \}) \cup \{ e_2 \} $ \\
                postavi $ E_2 = (E_2 \setminus \{ e_2 \}) \cup \{ e_1 \} $ \\
            } \uElse {
                ažuriraj vrednost $ p = \frac{1}{\sqrt{i}} $ \\
                generiši slučajan broj $ q $,  $ 0 \leq q \leq 1 $ \\
                
                \uIf{ $ p > q $ } {
                    postavi $ E_1 = E_1 \setminus \{ e_1 \} $ \\
                    postavi $ E_2 = E_2 \cup \{ e_1 \} $ \\
                }
            }
        } \\
    
        \uIf{ $ |E_1| = 3 |V| - 6 $ } {
            \Return $ E_1 $
        } \\
        \uIf{ $ |E_2| = 0 $ } {
            \Return $ E_1 $
        } \\
    }
    \Return $ E_1 $
\end{algorithm}
    \end{minipage}
\end{figure}

Provera uslova $ |E_1| = 3 |V| - 6 $ sledi iz Ojlerove teoreme \ref{theorem:Euler}, jer planaran
graf sa $ n $, $ n \geq 3 $,  čvorova može imati najviše $ 3n - 6 $ grana. 
Ovaj uslov nam omogućava da proverimo da li
smo postigli optimalno rešenje. Takođe, treba voditi računa o tome, da li ima još
grana u grafu $ E_2 $, ukoliko nema dobili smo rešenje. Takođe, za ispitivanje planarnosti,
kao što smo naveli u \ref{sec:graph-theory} osobini 2, važno je pronaći i ispitati cikluse
grafa. 


\subsection{ Implementacija }
\label{sec:implementation}

Za implementaciju našeg algoritma koristili smo \emph{ C++ } programski jezik. Za
komilaciju je korišćen \emph{ g++ } komilator verzije 7.5.0. Za test planarnosti smo koristili biblioteku 
\emph{ boost }, a samim tim i implementaciju grafova jer test planarnosti zahteva graf iz date
biblioteke. 


Kao što smo napomenuli u \ref{sec:graph-theory}, jedan od većih implementacionih izazova
predstavlja test planarnosti. Efikasni testovi planarnosti postižu vremensku složenost i
prostornu složeonost $ O(V) $ gde je $ V $ broj čvorova datog grafa \cite{planarity-test}.


Sledi opis efikasnog algoritma planarnosti iz reference \cite{planarity-test}.


Alogirtiam testa planarnosti počinje ispitivanje broja grana u grafu. Ukoliko je 
broj grana u grafu veći od $ 3V - 3 $, gde je $ V $ broj grana, tada graf nije planaran.
Zatim, graf se deli na komponente povezanosti. 


Komponenta povezanosti grafa je maksimalan povezan podgraf,
u smislu da ako uklonimo bilo koji čvor, podgraf će ostati povezan.
Teorema \ref{theorem:graph-planarity} nam omogućava dalji rad algoritma. 

\begin{theorem}
\label{theorem:graph-planarity}
    Graf je planaran ako i samo ako su njegove kompoente povezanosti planarne.
\end{theorem}


Za deljenje grafa na njegove komponente povezanosti postoje algoritmi u vremenu
$ O(V + E) $, gde je $ V $ broj čvorova a $ E $ broj grana grafa.


Za testiranje planarnosti svake od komponenti, prvo se primenjuje algoritam pretrage u 
dubinu kako bi pretvorili graf u stablo $ P $, nakon čega se čvorovi označe brojevima.
Zatim se stablu $P$ primenjuje algoritam koji pronalazi i briše cikluse, tako da imamo skup
nepovezanih delova grafa.


Zatim algoritam rekurzivno proverava planarnost svakog nepovezanog dela grafa, zajedno 
sa originalnim ciklusom. 
Na taj način određuje da li se delovi grafa mogu kombinovati tako da daju ceo graf.
Ukoliko mogu, graf je planaran.


Ukupna složenost celokupnog algoritma simuliranog kaljenja zavisi od testa planarnosti.
Kako je korišćen test planarnosti vremenske složenosti $ O(n) $, postignuta 
vremenska i prostorna složenost algoritma simuliranog kaljenja je $ O(ne)$, gde je konstantni
faktor veliki \cite{timo1}. 


\section{ Postignuti rezultati }
\label{sec:results}


Kako bi testirali naš algoritam, a zatim i uporedili rezultate, koristili smo isti skup
grafova kao što je navedeno u radu \cite{timo1}.
\footnote{ Podaci koji su korišćeni za testiranje se mogu preuzeti na 
\url{http://mauricio.resende.info/data/planar-data.tar.gz} }

U tabelama \ref{tabular:gs}, \ref{tabular:rgs} i \ref{tabular:tgs} se mogu videti postignuti
rezultati, tj. maksimalan broj
grana planarnog podgrafa koji je postignut algoritmom. Za neke grafove
je poznato optimalno rešenje, dok za one grafove za koje nije poznato optimalno rešenje
je navedena granica $ 3|V| - 6 $. Rezultati su poređeni sa rezultatima dobijenim u \cite{timo1}.


Korišćen kriterijum zaustavljanja je dostignut maksimalan broj evaluacija funkcije
\footnote{ Izvorni kod se može preuzeti na 
\url{https://gitlab.com/petar.perisic/maximum-planar-subgraph} }.
Maksimalan broj evaluacija funkcije se određuje po formuli $ \alpha * (|V| + |E|) $, gde je 
$ |V| $ broj čvorova ulaznog grafa a $ |E| $ broj grana ulaznog grafa.
Konstanta $ \alpha $ zavisi od broja čvorova ulaznog grafa $ |V| $. Kako je za što bolje i preciznije
rezultate presudan broj iteracija, za male grafove, tj grafove do 25 čvorova, uzeta je vrednost $ \alpha = 51 $.
Za ostale grafove uzeta vrednost za $ \alpha $ je $ 7 $.


Za veće vrednosti konstante $ \alpha $ algoritam pronalazi bolja rešenja, međutim tada je vreme izvršavanja
algoritma takođe veće. Kako bi pronašli balans između kvaliteta rešenja i vremena izvršavanja izabrane su date vrednosti.


Pri implementaciji algoritma, korišćen je i maksimalan broj iteracija kao kriterijum zaustavljanja.
Međutim, kako veći grafovi zahtevaju veći broj iteracija, za male grafove vreme izvršavanja
aloritama bilo bi veliko. U ovom slučaju za $ 1 000 000 $ iteracija i  graf \emph{ rg300.1 }
postignut je planaran podgraf sa  $ 394 $ grana što je  bolje od navedenog rešenja iz \cite{timo1}.


\begin{table}[H]
\centering
\begin{tabular}{c | c c c c c}
Graf & $ |V| $ & $ |E| $ & Optimum & Rešenje \cite{timo1} & Postignuto rešenje \\
\hline
g1  & 10  & 22   & 20  & 20  & 20 \\  
g2  & 45  & 85   & 24  & 82  & 82 \\ 
g3  & 10  & 24   & 24  & 24  & 24 \\ 
g4  & 10  & 25   & 24  & 24  & 24 \\ 
g5  & 10  & 26   & 24  & 24  & 24 \\ 
g6  & 10  & 27   & 24  & 24  & 24 \\ 
g7  & 10  & 34   & 24  & 24  & 24 \\ 
g8  & 25  & 69   & 69  & 24  & 69 \\ 
g9  & 25  & 70   & 69  & 69  & 69 \\ 
g10 & 25  & 71   & 69  & 69  & 69 \\ 
g11 & 25  & 72   & 69  & 69  & 69 \\ 
g12 & 25  & 90   & 69  & 69  & 66 \\ 
g13 & 50  & 367  & 144 & 137 & 88 \\ 
g14 & 50  & 491  & 144 & 143 & 89 \\ 
g15 & 50  & 582  & 144 & 144 & 88 \\ 
g16 & 100 & 451  & 294 & 206 & 144 \\ 
g17 & 100 & 742  & 294 & 244 & 159 \\ 
g18 & 100 & 922  & 294 & 257 & 148 \\ 
g19 & 150 & 1064 & 444 & 331 & 210 \\ 

\end{tabular}

\caption{ Graf test instance Goldšmita i Takvoriana }
\label{tabular:gs}
\end{table}


Kao što možemo primetiti za manje grafove, dobijamo optimalno rešenje, međutim za grafove
sa većim brojem čvorova i grana, ne postiže se opitmalno rešenje. Ukoliko bi se povećala
konstanta $ \alpha $ a samim tim i broj iteracija, broj grana bi bio veći. Međutim tada bi se
takođe značajno povećalo i vreme izvršavanja algoritma.


Treba napomenuti da se rad našeg algoritma ne ponaša dobro za grafove sa malim brojem 
čvorova i velikim brojem grana. U takvim slučajevima se ne pronalaze rešenja blizu optimuma.
Jedan od razloga je izabrana opadajuća funkcija $ \frac{1}{\sqrt{i}}$, gde je $ i $ broj trenutne
iteracije. Kako za velike vrednosti broja $ i $, ova funkcija teži 0, retko će dolaziti do 
samog vraćanja slučajno odabrane grane iz skupa $ E1 $ u skup $ E2 $. Pored ove funkcije, 
koristili smo i $ \frac{ln2}{ln(1 + i)} $ ali ona nije davala bolje rezultate.


U tabeli \ref{tabular:rgs} možemo videti slično ponašanje algoritma, dok
u tabeli \ref{tabular:tgs} možemo videti da se naš algoritam i nije loše pokazao.
Za grafove poput \emph{tg.100.1} i \emph{tg.200.1} pronašao optimalna rešenja. 
Iako je u drugim testovima pronašao nešto slabija rešenja, možemo reći da se za
ove test instance pokazao algoritam dobro.


Zavisno od veličine grafa i vremena koje je potrebno za njegovu obradu, puštan je određen broj 
instanci. Za grafove do $ 150 $ grana puštano je $ 20 $  instanci, za grafove od $ 150 $ grana puštano 
je $ 10 $ instanci, dok je za grafove od  $200 $ do $ 300 $ grana puštano po 5 instanci.


\begin{table}[H]
\centering
\begin{tabular}{c | c c c c c}
Graf & $ |V| $ & $ |E| $ & Optimum & Rešenje \cite{timo1} & Postignuto rešenje \\
\hline

g1.cimi & 10 & 21  & 19  & 19  & 19 \\ 
g2.cimi & 60 & 166 & 165 & 165 & 165 \\ 
g3.cimi & 28 & 75  & 73  & 73  & 73 \\ 
g4.cimi & 10 & 22  & 20  & 20  & 20 \\ 
g5.cimi & 45 & 85  & 82  & 82  & 82 \\ 
g6.cimi & 43 & 63  & 59  & 59  & 59 \\ 

\hline

rg50.1 & 50 & 123 & 144 & 94  & 82 \\ 
rg50.2 & 50 & 145 & 144 & 99  & 78 \\ 
rg50.3 & 50 & 157 & 144 & 106 & 86 \\ 
rg50.4 & 50 & 171 & 144 & 108 & 88 \\ 
rg50.5 & 50 & 183 & 144 & 110 & 84 \\ 

\hline

rg75.1 & 75 & 196 & 219 & 136 & 113 \\ 
rg75.2 & 75 & 202 & 219 & 139 & 116 \\ 
rg75.3 & 75 & 215 & 219 & 141 & 113 \\ 
rg75.4 & 75 & 256 & 219 & 152 & 115 \\ 
rg75.5 & 75 & 266 & 219 & 153 & 115 \\ 

\hline

rg100.1 & 100 & 261 & 294 & 170 & 137 \\ 
rg100.2 & 100 & 271 & 294 & 174 & 137 \\ 
rg100.3 & 100 & 297 & 294 & 179 & 137 \\ 
rg100.4 & 100 & 334 & 294 & 186 & 138 \\ 
rg100.5 & 100 & 373 & 294 & 197 & 143 \\ 

\hline

rg150.1 & 150 & 387 & 444 & 243 & 196 \\ 
rg150.2 & 150 & 402 & 444 & 241 & 203 \\ 
rg150.3 & 150 & 453 & 444 & 254 & 199 \\ 
rg150.4 & 150 & 473 & 444 & 258 & 193 \\ 
rg150.5 & 150 & 481 & 444 & 261 & 207 \\ 

\hline

rg200.1 & 200 & 514 & 594 & 308 & 259 \\ 
rg200.2 & 200 & 519 & 594 & 307 & 249 \\ 
rg200.3 & 200 & 644 & 594 & 334 & 271 \\ 
rg200.4 & 200 & 684 & 594 & 336 & 259 \\ 
rg200.5 & 200 & 701 & 594 & 342 & 254 \\ 

\hline

rg300.1 & 300 & 814  & 894 & 453 & 388 \\ 
rg300.2 & 300 & 1159 & 894 & 503 & 409 \\ 
rg300.3 & 300 & 1176 & 894 & 490 & 380 \\ 
rg300.4 & 300 & 1474 & 894 & 530 & 403 \\ 
rg300.5 & 300 & 1507 & 894 & 525 & 393 \\ 

\end{tabular}

\caption{ Graf test instance Kamikovskog, prvi deo }
\label{tabular:rgs}
\end{table}


\begin{table}[H]
\centering
\begin{tabular}{c | c c c c c}
Graf & $ |V| $ & $ |E| $ & Optimum & Rešenje \cite{timo1} & Postignuto rešenje \\
\hline

tg100.1  & 100 & 304 & 294 & 294 & 294 \\ 
tg100.2  & 100 & 314 & 294 & 294 & 290 \\ 
tg100.3  & 100 & 324 & 294 & 294 & 284 \\ 
tg100.4  & 100 & 334 & 294 & 294 & 288 \\ 
tg100.5  & 100 & 344 & 294 & 294 & 272 \\ 
tg100.6  & 100 & 354 & 294 & 293 & 274 \\ 
tg100.7  & 100 & 364 & 294 & 294 & 260 \\ 
tg100.8  & 100 & 374 & 294 & 294 & 249 \\ 
tg100.9  & 100 & 384 & 294 & 290 & 240 \\ 
tg100.10 & 100 & 394 & 294 & 290 & 230 \\ 

\hline

tg200.1  & 200 & 604 & 594 & 594 & 594 \\ 
tg200.2  & 200 & 614 & 594 & 594 & 594 \\ 
tg200.3  & 200 & 624 & 594 & 594 & 593 \\ 
tg200.4  & 200 & 634 & 594 & 594 & 592 \\ 
tg200.5  & 200 & 644 & 594 & 594 & 588 \\ 
tg200.6  & 200 & 654 & 594 & 594 & 589 \\ 
tg200.7  & 200 & 664 & 594 & 594 & 588 \\ 
tg200.8  & 200 & 674 & 594 & 594 & 588 \\ 
tg200.9  & 200 & 684 & 594 & 594 & 580 \\ 
tg200.10 & 200 & 694 & 594 & 594 & 578 \\ 

\end{tabular}

\caption{ Graf test instance Kamikovskog, drugi deo }
\label{tabular:tgs}
\end{table}


U tabelama \ref{tabular:time-gs}, \ref{tabular:time-rgs} i \ref{tabular:time-tgs} 
se mogu videti prosečna vremena dobijena našim algoritmom i prosečna vremena izvršavanja algoritma 
dobijena u \cite{timo1}.
Za merenje vrena smo koristili \emph{ C++ \ std::chrono } biblioteku,
tako što smo oduzeli krajnje i početno vreme izvršavanja testa. Sva vremena su merena u sekundama.

\begin{table}[H]
\centering
\begin{tabular}{c | c c c c c c}
Graf & $ |V| $ & $ |E| $ & Prosečno vreme \cite{timo1} & Prosečno vreme 
& Rešenje \cite{timo1} & Rešenje \\

\hline

g1  & 10  & 22   & 2 & 5   & 20  & 20 \\ 
g2  & 45  & 85   & 0 & 34  & 82  & 82 \\ 
g3  & 10  & 24   & 0 & 0    & 24  & 24 \\ 
g4  & 10  & 25   & 0 & 0   & 24  & 24 \\ 
g5  & 10  & 26   & 0 & 0   & 24  & 24 \\ 
g6  & 10  & 27   & 0 & 1   & 24  & 24 \\ 
g7  & 10  & 34   & 0 & 0    & 24  & 24 \\ 
g8  & 25  & 69   & 0 & 0    & 24  & 69 \\ 
g9  & 25  & 70   & 0 & 0    & 69  & 69 \\ 
g10 & 25  & 71   & 0 & 0   & 69  & 69 \\ 
g11 & 25  & 72   & 0 & 10   & 69  & 69 \\ 
g12 & 25  & 90   & 12 & 244 & 69  & 66 \\ 
g13 & 50  & 367  & 319 & 195  & 137 & 88 \\ 
g14 & 50  & 491  & 481 & 306  & 143 & 89 \\ 
g15 & 50  & 582  & 580 & 449  & 144 & 88 \\ 
g16 & 100 & 451  & 720 & 521  & 206 & 144 \\ 
g17 & 100 & 742  & 1471 & 1249 & 244 & 159 \\ 
g18 & 100 & 922  & 1985 & 529  & 257 & 148 \\ 
g19 & 150 & 1064 & 3705 & 1238 & 331 & 210 \\ 

\end{tabular}

\caption{ Graf test instance Goldšmita i Takvoriana }
\label{tabular:time-gs}
\end{table}

Kao što možemo videti u tabeli \ref{tabular:time-gs} za male grafove postižemo, pored optimalnih 
rešenja, isto prosečno izvršavanje algoritma, sa izuzetkom grafa \emph{g2}. Za veće grafove, iako
dobijamo gora rešenja, dobijamo bolja prosečna vremena izvršavanja programa. Kao što smo ranije napomenuli,
tražili smo balans između vremena izvršavanja algoritma i kvaliteta rešenja. O rezultatima u tabelama
\ref{tabular:time-rgs}  i \ref{tabular:time-tgs} možemo doneti isti zaključak. 
Treba napomenuti da za manje grafove, pored što postižemo optimalno rešenje,
u određenim test instancama postžemo i bolje prosečno izvršavanje algoritma.

\begin{table}[H]
\centering
\begin{tabular}{c | c c c c c c}
Graf & $ |V| $ & $ |E| $ & Prosečno vreme \cite{timo1} & Prosečno vreme 
& Rešenje \cite{timo1} & Rešenje \\

\hline

g1.cimi & 10 & 21  & 3 & 2     & 19  & 19 \\ 
g2.cimi & 60 & 166 & 147 & 98 & 165 & 165 \\ 
g3.cimi & 28 & 75  & 28 & 2    & 73  & 73 \\ 
g4.cimi & 10 & 22  & 3 & 7     & 20  & 20 \\ 
g5.cimi & 45 & 85  & 42 & 7   & 82  & 82 \\ 
g6.cimi & 43 & 63  & 26 & 2   & 59  & 59 \\ 

\hline
rg50.1 & 50 & 123 & 67 & 34   & 94  & 82 \\ 
rg50.2 & 50 & 145 & 84 & 39  & 99  & 78 \\ 
rg50.3 & 50 & 157 & 99 & 47  & 106 & 86 \\ 
rg50.4 & 50 & 171 & 108 & 53 & 108 & 88 \\ 
rg50.5 & 50 & 183 & 110 & 59  & 110 & 84 \\ 

\hline
rg75.1 & 75 & 196 & 197 & 76 & 136 & 113 \\ 
rg75.2 & 75 & 202 & 216 & 90  & 139 & 116 \\ 
rg75.3 & 75 & 215 & 206 & 90  & 141 & 113 \\ 
rg75.4 & 75 & 256 & 295 & 153 & 152 & 115 \\ 
rg75.5 & 75 & 266 & 316 & 128 & 153 & 115 \\ 

\hline
rg100.1 & 100 & 261 & 338 & 120 & 170 & 137 \\ 
rg100.2 & 100 & 271 & 333 & 126  & 174 & 137 \\ 
rg100.3 & 100 & 297 & 405 & 152 & 179 & 137 \\ 
rg100.4 & 100 & 334 & 435 & 151  & 186 & 138 \\ 
rg100.5 & 100 & 373 & 561 & 207 & 197 & 143 \\ 

\hline
rg150.1 & 150 & 387 & 1000 & 300 & 243 & 196 \\ 
rg150.2 & 150 & 402 & 1157 & 362 & 241 & 203 \\ 
rg150.3 & 150 & 453 & 1216 & 523 & 254 & 199 \\ 
rg150.4 & 150 & 473 & 1291 & 453 & 258 & 193 \\ 
rg150.5 & 150 & 481 & 1304 & 612 & 261 & 207 \\ 

\hline
rg200.1 & 200 & 514 & 1620 & 902  & 308 & 259 \\ 
rg200.2 & 200 & 519 & 1638 & 783  & 307 & 249 \\ 
rg200.3 & 200 & 644 & 2133 & 1746 & 334 & 271 \\ 
rg200.4 & 200 & 684 & 2400 & 1069 & 336 & 259 \\ 
rg200.5 & 200 & 701 & 2500 & 927 & 342 & 254 \\ 

\hline
rg300.1 & 300 & 814  & 6131 & 2204  & 453 & 388 \\ 
rg300.2 & 300 & 1159 & 9600 & 3972  & 503 & 409 \\ 
rg300.3 & 300 & 1176 & 9226 & 2647  & 490 & 380 \\ 
rg300.4 & 300 & 1474 & 13117 & 4302 & 530 & 403 \\ 
rg300.5 & 300 & 1507 & 13781 & 3826 & 525 & 393 \\ 


\end{tabular}

\caption{ Graf test instance Kamikovskog, prvi deo }
\label{tabular:time-rgs}
\end{table}

\begin{table}[H]
\centering
\begin{tabular}{c | c c c c c c}
Graf & $ |V| $ & $ |E| $ & Prosečno vreme \cite{timo1} & Prosečno vreme 
& Rešenje \cite{timo1} & Rešenje \\

\hline

tg100.1  & 100 & 304 & 88  & 125  & 294 & 294 \\ 
tg100.2  & 100 & 314 & 34  & 400  & 294 & 290 \\ 
tg100.3  & 100 & 324 & 132 & 218  & 294 & 284 \\ 
tg100.4  & 100 & 334 & 596 & 332  & 294 & 288 \\ 
tg100.5  & 100 & 344 & 410 & 362  & 294 & 272 \\ 
tg100.6  & 100 & 354 & 843 & 371 & 293 & 274 \\ 
tg100.7  & 100 & 364 & 721 & 356  & 294 & 260 \\ 
tg100.8  & 100 & 374 & 766 & 359  & 294 & 249 \\ 
tg100.9  & 100 & 384 & 837 & 374  & 290 & 240 \\ 
tg100.10 & 100 & 394 & 873 & 324  & 290 & 230 \\ 

\hline

tg200.1  & 200 & 604 & 79   & 126  & 594 & 594 \\ 
tg200.2  & 200 & 614 & 229  & 1099 & 594 & 594 \\ 
tg200.3  & 200 & 624 & 458  & 2900 & 594 & 593 \\ 
tg200.4  & 200 & 634 & 1975 & 2896 & 594 & 592 \\ 
tg200.5  & 200 & 644 & 1560 & 2593 & 594 & 588 \\ 
tg200.6  & 200 & 654 & 1990 & 2610 & 594 & 589 \\ 
tg200.7  & 200 & 664 & 2542 & 2853 & 594 & 588 \\ 
tg200.8  & 200 & 674 & 3058 & 3376 & 594 & 588 \\ 
tg200.9  & 200 & 684 & 2606 & 2603 & 594 & 580 \\ 
tg200.10 & 200 & 694 & 3613 & 2601 & 594 & 578 \\ 


\end{tabular}

\caption{ Graf test instance Kamikovskog, drugi deo }
\label{tabular:time-tgs}
\end{table}

\section{ Zaključak }


U ovom radu smo pokazali kako se simulirano kaljenje može primeniti na problem maksimalnog
planarnog podgrafa. Rezultati koje smo prikazali u ovom radu pokazuju da se naša implementacija
algoritma pokazuje dobro za grafove sa malim brojem čvorova, 
gde se pored dobrog prosečnog vremena izvršavanja dostiže i optimalno rešenje. 
U sekciji \ref{sec:annealing-impl} smo pokazali da je i sama implementacija
algoritma jednostavna i intuitivna.


Kako bi se postigli bolji rezultati, u samom algoritmu je 
moguće pronaći bolje parametre u kriterijumu zaustavljanja
ili koristiti drugi kriterijum zaustavljanja.


\clearpage
\addcontentsline{toc}{section}{Literatura}

\bibliography{references}
\bibliographystyle{plain}

\end{document}
